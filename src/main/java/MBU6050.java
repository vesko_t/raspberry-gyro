import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import util.Matrix3f;
import util.Vector3f;

import java.io.IOException;

public class MBU6050 {
    private static final int PWR = 0x6B;
    private static final int CONFIG = 0x1A;
    private static final int GYRO_CONFIG = 0x1B;
    private static final int ACCEL_CONFIG = 0x1C;
    private static final int ACCEL_X = 0x3B;
    private static final int ACCEL_Y = 0x3D;
    private static final int ACCEL_Z = 0x3F;
    private static final int GYRO_X = 0x43;
    private static final int GYRO_Y = 0x45;
    private static final int GYRO_Z = 0x47;
    private static final int INTERRUPT_CONFIG = 0x37;
    private static final int INTERRUPT_ENABLE = 0x38;

    public static final int GYRO_200 = 0x00;
    public static final int GYRO_500 = 0x08;
    public static final int GYRO_1000 = 0x10;
    public static final int GYRO_2000 = 0x18;

    private static float gyroScaleFactor;

    private I2CBus i2CBus;
    private I2CDevice i2CDevice;
    private Vector3f gyroData = new Vector3f();
    private Vector3f accelerationData = new Vector3f();
    private Vector3f angle = new Vector3f();
    private Vector3f rotatedAcceleration = new Vector3f();

    private Vector3f accelerationOffset = new Vector3f();
    private Vector3f gyroOffset = new Vector3f();
    private Matrix3f rotation = new Matrix3f();

    public MBU6050(int gyroSensitivity) {
        this(0x68, gyroSensitivity);
    }

    public MBU6050(int address, int gyroSensitivity) {
        try {
            i2CBus = I2CFactory.getInstance(1);
            i2CDevice = i2CBus.getDevice(address);
            i2CDevice.write(CONFIG, (byte) 0);
            i2CDevice.write(GYRO_CONFIG, (byte) gyroSensitivity);
            i2CDevice.write(ACCEL_CONFIG, (byte) 0x00);
            i2CDevice.write(PWR, (byte) 0x00);
            i2CDevice.write(INTERRUPT_ENABLE, (byte) 1);
            //i2CDevice.write(0x37,);
            switch (gyroSensitivity) {
                case GYRO_200:
                    gyroScaleFactor = 131.0f;
                    break;
                case GYRO_500:
                    gyroScaleFactor = 65.5f;
                    break;
                case GYRO_1000:
                    gyroScaleFactor = 32.8f;
                    break;
                case GYRO_2000:
                    gyroScaleFactor = 16.4f;
                    break;
            }
            calibrateAccelerometer();
            calibrateGyro();
            System.out.println("Calibrated");
        } catch (IOException | I2CFactory.UnsupportedBusNumberException e) {
            e.printStackTrace();
        }
    }

    private int readWord(int address) throws IOException {
        int value = i2CDevice.read(address);
        value = value << 8;
        value += i2CDevice.read(address + 1);
        if (value >= 0x8000)
            value = -(65536 - value);
        return value;
    }

    private Vector3f getAccelerationData() throws IOException {
        int acclX = readWord(ACCEL_X);
        int acclY = readWord(ACCEL_Y);
        int acclZ = readWord(ACCEL_Z);

        return new Vector3f(acclX, acclY, acclZ);
    }

    private Vector3f getGyroData() throws IOException {
        int gyroX = readWord(GYRO_X);
        int gyroY = readWord(GYRO_Y);
        int gyroZ = readWord(GYRO_Z);

        return new Vector3f(gyroX, gyroY, gyroZ);
    }

    public Vector3f getRotatedAcceleration() {
        return rotatedAcceleration;
    }

    public Vector3f getAngularVelocity() {
        return gyroData.div(gyroScaleFactor, new Vector3f()).sub(gyroOffset);
    }

    public Vector3f getAcceleration() {
        return accelerationData.div(16384.0f, new Vector3f()).sub(accelerationOffset);
    }

    public Vector3f getAngle() {
        return angle;
    }

    public void update() throws IOException {
        gyroData = getGyroData();
        accelerationData = getAccelerationData();
    }

    private void calibrateAccelerometer() throws IOException {
        accelerationOffset = new Vector3f();
        for (int i = 0; i < 1000; i++) {
            accelerationOffset.add(getAccelerationData().div(16384.0f));
        }
        accelerationOffset.div(1000);
    }

    private void calibrateGyro() throws IOException {
        gyroOffset = new Vector3f();
        for (int i = 0; i < 1000; i++) {
            gyroOffset.add(getGyroData().div(gyroScaleFactor));
        }
        gyroOffset.div(1000);
    }
}
