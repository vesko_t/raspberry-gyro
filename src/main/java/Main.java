import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.pi4j.wiringpi.GpioInterruptCallback;
import util.Vector3f;

import static com.pi4j.wiringpi.Gpio.*;

public class Main {

    private static MBU6050 mbu6050;
    private static long lastInterrupt;
    private static float delta;
    private static Vector3f velocity = new Vector3f();
    private static Vector3f position = new Vector3f();
    private Socket clientSocket;
    private OutputStream outputStream;

    public Main() throws IOException {
        if (wiringPiSetup() == -1) {
            System.err.println("WiringPi error");
            System.exit(1);
        }

        mbu6050 = new MBU6050(MBU6050.GYRO_200);
        clientSocket = new Socket("192.168.253.80", 8080);
        outputStream = clientSocket.getOutputStream();

        PrintWriter printWriter = new PrintWriter(new File("values.csv"));

        printWriter.println("Accelerometer, , ");
        printWriter.println("x, y, z");

        lastInterrupt = System.nanoTime();
        wiringPiISR(0, INT_EDGE_RISING, new GpioInterruptCallback() {
            @Override
            public void callback(int pin) {
                try {
                    long now = System.nanoTime();
                    double delta = (now - lastInterrupt) / 1e9d;
                    lastInterrupt = now;
                    mbu6050.update();
                    Vector3f acceleration = mbu6050.getAcceleration().div(9.81f, new Vector3f());
                    velocity.add(acceleration.mul((float) delta));
                    position.add(velocity.mul((float) delta));
                    System.out.println(position.toString());
                    outputStream.write(position.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        while (true) {

        }
    }

    public static void main(String... args) throws IOException {
        new Main();
    }

}
