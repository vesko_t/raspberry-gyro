import util.Vector3f;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class Sender implements Runnable {

    private Vector3f angle;
    private float delta;
    private OutputStream outputStream;

    public Sender(Vector3f angle, OutputStream outputStream) {
        this.angle = angle;
        this.outputStream = outputStream;
    }

    @Override
    public void run() {
        try {
            outputStream.write(new Vector3f().toByteArray());
            outputStream.write(angle.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
