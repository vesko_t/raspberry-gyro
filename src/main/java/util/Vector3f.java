package util;

import java.nio.ByteBuffer;

public class Vector3f {

    public float x;
    public float y;
    public float z;

    public Vector3f() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3f(float value) {
        this.x = value;
        this.y = value;
        this.z = value;
    }

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3f(Vector3f value) {
        this(value.x, value.y, value.z);
    }

    public float x() {
        return x;
    }

    public float y() {
        return y;
    }

    public float z() {
        return z;
    }

    public Vector3f add(Vector3f v) {
        return add(v, this);
    }

    public Vector3f add(Vector3f v, Vector3f dest) {
        dest.x = x + v.x;
        dest.y = y + v.y;
        dest.z = z + v.z;
        return dest;
    }

    public Vector3f sub(Vector3f v) {
        return sub(v, this);
    }

    public Vector3f sub(Vector3f v, Vector3f dest) {
        dest.x = x + v.x;
        dest.y = y + v.y;
        dest.z = z + v.z;
        return dest;
    }

    public Vector3f div(float v) {
        return div(v, this);
    }

    public Vector3f div(float v, Vector3f dest) {
        dest.x = x / v;
        dest.y = y / v;
        dest.z = z / v;
        return dest;
    }

    public Vector3f div(Vector3f v) {
        return div(v, this);
    }

    public Vector3f div(Vector3f v, Vector3f dest) {
        dest.x = x / v.x;
        dest.y = y / v.y;
        dest.z = z / v.z;
        return dest;
    }

    public Vector3f mul(Vector3f v) {
        return mul(v, this);
    }

    public Vector3f mul(Vector3f v, Vector3f dest) {
        dest.x = this.x * v.x;
        dest.y = this.y * v.y;
        dest.z = this.z * v.z;
        return dest;
    }

    public Vector3f mul(float scalar) {
        return mul(scalar, this);
    }

    public Vector3f mul(float scalar, Vector3f dest) {
        dest.x = x * scalar;
        dest.y = y * scalar;
        dest.z = z * scalar;
        return dest;
    }


    public Vector3f rotate(Quaternionf quat) {
        return rotate(quat, this);
    }

    public Vector3f rotate(Quaternionf quat, Vector3f dest) {
        return quat.transform(this, dest);
    }


    public byte[] toByteArray() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(12);
        byteBuffer.putFloat(x).putFloat(y).putFloat(z);
        byteBuffer.rewind();
        return byteBuffer.array();
    }

    public float lengthSquared() {
        return lengthSquared(x, y, z);
    }

    public static float lengthSquared(float x, float y, float z) {
        return x * x + y * y + z * z;
    }

    public float length() {
        return (float) Math.sqrt(lengthSquared());
    }

    public static float length(float x, float y, float z) {
        return (float) Math.sqrt(lengthSquared(x, y, z));
    }

    public Vector3f normalize() {
        return normalize(this);
    }

    public Vector3f normalize(Vector3f dest) {
        float invLength = 1.0f / length();
        dest.x = x * invLength;
        dest.y = y * invLength;
        dest.z = z * invLength;
        return dest;
    }

    public Vector3f mul(Matrix3f mat) {
        return mul(mat, this);
    }

    /* (non-Javadoc)
     * @see org.joml.Vector3fc#mul(org.joml.Matrix3fc, org.joml.Vector3f)
     */
    public Vector3f mul(Matrix3f mat, Vector3f dest) {
        float rx = mat.m00() * x + mat.m10() * y + mat.m20() * z;
        float ry = mat.m01() * x + mat.m11() * y + mat.m21() * z;
        float rz = mat.m02() * x + mat.m12() * y + mat.m22() * z;
        dest.x = rx;
        dest.y = ry;
        dest.z = rz;
        return dest;
    }

    @Override
    public String toString() {
        return "X: " + x + "Y: " + y + "Z: " + z;
    }
}
